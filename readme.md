Sample size planning with brms
================
Matti Vuorre
6/28/2018

*This document is an early version of work in progress. Please get in
touch if you have questions or comments.*

# Preface

> “Statistical *power* is the probability of achieving the goal of a
> planned empirical study, if a suspected underlying state of the world
> is true. Scientists don’t want to waste time and resources pursuing
> goals that have a small probability of being achieved. In other words,
> researchers desire power.” (Kruschke 2014)

Here, I briefly describe how to plan for a sufficient sample size for a
longitudinal study. Let’s face it, standard power calculations are of
little use when we leave the world of ANOVA and t-tests behind: more
complicated models make exact power calculations difficult, and we must
rely on simulation. An additional issue with frequentist (and indeed all
power analysis with the aim of statistical significance) power analysis
is that you start with a point estimate of the hypothetical effect size.
There’s no uncertainty in your hypothesis? Get out of here. \[Also,
standardized effect sizes. Are you kidding me?\] So here we take a look
at power analysis for a common but more complex model, in both
frequentist and Bayesian frameworks.

Our starting point is a study described in (Bolger and Laurenceau 2013,
Chapter 4): 50 females were split into two groups (treatment and
control), and their self-reported intimacy ratings were tracked over 16
weeks. The hypothesis was that females in the treatment group would show
a greater increase in intimacy over time than females in the control
group. I have included the data as a .csv file in this repo, and first
load the data into R, and visualize the main trends in the data. The
data is available for download at <http://intensivelongitudinal.com/>

``` r
library(tidyverse)
library(lme4)
library(simr)
library(brms)
options(mc.cores = parallel::detectCores())
theme_set(theme_linedraw() + theme(panel.grid = element_blank()))
```

``` r
dat <- read_csv("time.csv")
dat
```

    ## # A tibble: 800 x 5
    ##       id  time time01 intimacy treatment
    ##    <int> <int>  <dbl>    <dbl>     <int>
    ##  1     1     0 0          2.96         0
    ##  2     1     1 0.0667     2.34         0
    ##  3     1     2 0.133      4.88         0
    ##  4     1     3 0.2        2.99         0
    ##  5     1     4 0.267      3.13         0
    ##  6     1     5 0.333      2.73         0
    ##  7     1     6 0.4        1.96         0
    ##  8     1     7 0.467      4.13         0
    ##  9     1     8 0.533      3.17         0
    ## 10     1     9 0.6        2.93         0
    ## # ... with 790 more rows

``` r
p_dat <- dat %>% 
  ggplot(aes(time, intimacy)) +
  geom_smooth(aes(group=id), method=lm, se = F, col = "black", size = .5) +
  facet_wrap("treatment", labeller = label_both) +
  labs(x = "Week", y = "Intimacy", caption = "OLS model regression lines")
p_dat
```

<img src="readme_files/figure-gfm/plot-trends-1.png" width="672" />

The post-hoc power analysis for these data is described in (Bolger and
Laurenceau 2013, Chapter 10). Before the post-hoc power analysis, we
define our model, which is a multilevel model with a linear trend of
time (`time01`, which recodes the time variable such that 0 is the first
day of measurement, and 1 is the last), an effect of `treatment` group,
and their interaction. Intercepts and effects of time are specified as
varying between subjects (there are repeated measures of intimacy over
time for every participant.) The parameter of main interest is the
`time01:treatment` interaction. We hypothesized that this parameter
would be positive.

``` r
fit_lmer <- lmer(intimacy ~ time01*treatment + (time01 | id),
                 data = dat)
summary(fit_lmer)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: intimacy ~ time01 * treatment + (time01 | id)
    ##    Data: dat
    ## 
    ## REML criterion at convergence: 2834.4
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -2.6242 -0.6798 -0.0190  0.6434  2.4463 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev. Corr 
    ##  id       (Intercept) 0.6858   0.8281        
    ##           time01      1.8936   1.3761   -0.45
    ##  Residual             1.6925   1.3010        
    ## Number of obs: 800, groups:  id, 50
    ## 
    ## Fixed effects:
    ##                  Estimate Std. Error t value
    ## (Intercept)       2.89897    0.20703  14.003
    ## time01            0.73520    0.34720   2.118
    ## treatment        -0.05644    0.29279  -0.193
    ## time01:treatment  0.92143    0.49101   1.877
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) time01 trtmnt
    ## time01      -0.599              
    ## treatment   -0.707  0.423       
    ## tm01:trtmnt  0.423 -0.707 -0.599

Although not necessarily the best approach, here we focus on rejecting
the null hypothesis. Ignore the details for now. The critical p-value
from this model is

``` r
# From simr package
doTest(fit_lmer, fixed("time01:treatment", "kr"))
```

    ## p-value for predictor 'time01:treatment': 0.06666024
    ##           --------------------
    ## Test: Kenward Roger (package pbkrtest)
    ##       Effect size for time01:treatment is 0.92

# Frequentist power analysis

As a first step, we reproduce the power analysis in (Bolger and
Laurenceau 2013, Chapter 10). The authors used Mplus, but we instead use
the the **simr** R package (Green and MacLeod
2015).

``` r
res_simr <- powerSim(fit_time, fixed("time01:treatment", "kr"), nsim = 500)
res_simr
```

    ## Power for predictor 'time01:treatment', (95% confidence interval):
    ##       47.20% (42.75, 51.68)
    ## 
    ## Test: Kenward Roger (package pbkrtest)
    ##       Effect size for time01:treatment is 0.92
    ## 
    ## Based on 500 simulations, (0 warnings, 0 errors)
    ## alpha = 0.05, nrow = 800
    ## 
    ## Time elapsed: 0 h 3 m 50 s
    ## 
    ## nb: result might be an observed power calculation

Our simulated power estimate for this study to detect a significant
(\(\alpha = .05\)) interaction is .472 (95%CI based on 500 simulations
\[42.75, 51.68\]). (Bolger and Laurenceau 2013) reported a power
estimate of .466, so we are very close, and I suspect the small
difference is due to randomness inherent to the monte carlo sampling,
and possibly a different method for obtaining the p-value (we used the
Kenward-Roger method, and the authors used Mplus which may use another
method.) Our next step is to reproduce the post-hoc power analysis using
Bayesian methods, namely the brms R package (Bürkner 2017).

# Bayesian planning

## Outline

The details of Bayesian sample size planning are described in (Kruschke
2014, Chapter 13). I won’t explain the details here, but the main steps
are:

1.  Generate representative parameter values from hypothesis

This is similar to picking a target effect size for a frequentist power
analysis, but instead of a point estimate, our target effect size is
represented by an entire distribution of possible parameter values. In a
post-hoc power analysis, the representative parameter values are picked
from the posterior distribution of a model fitted to real data. This is
what we are going to do.

2.  Generate a random sample of data from the hypothetical parameter
    values

Generate a fake dataset from one set of representative parameter values.
(That is, take one row of MCMC samples from the posterior distribution,
and predict one dataset.)

3.  Estimate the posterior distribution of the model based on the fake
    dataset.

Refit the model to the fake dataset. Summarise the model.

4.  Tally whether goal was attained

There are many possible goals that we might want to plan our study for.
For simplicity, our goal here is to exclude zero from the 95%
uncertainty interval. Clearly not an optimal goal, but it ties in nicely
with the not-great frequentist goal of getting a p \< .05.

5.  Repeat many times

For each repeat, save a record of whether your goal(s) was attained.
After several hundred/thousand iterations, calculate the proportion of
iterations where your goal was achieved. That is power.

These steps are explained in much more detail in (Kruschke 2014, Chapter
13). Let’s look at how to do this in practice with our current example
data and model.

First, we fit a model to the intimacy data set. This model’s posterior
distribution will be our hypothesis on which we base our estimated
effect sizes. In other words we’re going to be doing a post-hoc power
analysis.

``` r
fit_brm <- brm(intimacy ~ time01*treatment + (time01 | id),
               data = dat)
summary(fit_brm)
```

    ##  Family: gaussian 
    ##   Links: mu = identity; sigma = identity 
    ## Formula: intimacy ~ time01 * treatment + (time01 | id) 
    ##    Data: dat (Number of observations: 800) 
    ## Samples: 4 chains, each with iter = 2000; warmup = 1000; thin = 1;
    ##          total post-warmup samples = 4000
    ## 
    ## Group-Level Effects: 
    ## ~id (Number of levels: 50) 
    ##                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
    ## sd(Intercept)             0.84      0.14     0.59     1.14       1477 1.00
    ## sd(time01)                1.39      0.24     0.96     1.89        774 1.00
    ## cor(Intercept,time01)    -0.40      0.19    -0.70     0.03        824 1.00
    ## 
    ## Population-Level Effects: 
    ##                  Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
    ## Intercept            2.90      0.22     2.49     3.33       1839 1.00
    ## time01               0.72      0.36     0.03     1.44       1281 1.00
    ## treatment           -0.06      0.30    -0.66     0.52       1779 1.00
    ## time01:treatment     0.93      0.51    -0.06     1.88       1312 1.00
    ## 
    ## Family Specific Parameters: 
    ##       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
    ## sigma     1.30      0.04     1.24     1.38       4000 1.00
    ## 
    ## Samples were drawn using sampling(NUTS). For each parameter, Eff.Sample 
    ## is a crude measure of effective sample size, and Rhat is the potential 
    ## scale reduction factor on split chains (at convergence, Rhat = 1).

This saved object now contains our hypothesis of the effect size that we
are going to plan for, including all other relevant parameters and
uncertainties. Now, let’s do one iteration of steps 2-4.

2.  Generate a random sample of data from the hypothesis

But how? We use **brms**’ `predict()` method, which predicts the model’s
outcomes. Here, we only want to generate a single fake dataset. The
syntax is as follows:

``` r
fakeset1 <- predict(fit_brm, summary = F, subset = 1)
```

By specifying `subset = 1`, we generated a single vector of hypothetical
outcomes by using only the first row of the posterior sample matrix. See
`?predict.brmsfit`. I’ll put the outcome vector into a new dataframe
next to the original predictor values so that we can visualize it:

``` r
dat_new <- dat
dat_new$intimacy = fakeset1[1,]  # fakeset1 is a matrix, we want its first row
p_dat %+% dat_new  # Replace data of previous plot with this new data
```

<img src="readme_files/figure-gfm/plot-fake-single-1.png" width="672" />

Cool. Compare this figure to the figure with the original data. You
should see that the main trends—if you can see trends in the
spaghetti—are replicated.

3.  Estimate the model with the new fake dataset.

This is as easy as using the `update()` method, and passing the new fake
dataset as an argument:

``` r
fit_fake <- update(
  fit_brm,
  newdata = dat_new,
  recompile = F  # Prevent recompiling Stan code to C++ (saves time)
)
summary(fit_fake)
```

    ##  Family: gaussian 
    ##   Links: mu = identity; sigma = identity 
    ## Formula: intimacy ~ time01 * treatment + (time01 | id) 
    ##    Data: dat_new (Number of observations: 800) 
    ## Samples: 4 chains, each with iter = 2000; warmup = 1000; thin = 1;
    ##          total post-warmup samples = 4000
    ## 
    ## Group-Level Effects: 
    ## ~id (Number of levels: 50) 
    ##                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
    ## sd(Intercept)             0.93      0.15     0.67     1.25       2071 1.00
    ## sd(time01)                1.24      0.24     0.79     1.74       1169 1.00
    ## cor(Intercept,time01)    -0.41      0.19    -0.71     0.00       1473 1.00
    ## 
    ## Population-Level Effects: 
    ##                  Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
    ## Intercept            3.18      0.22     2.74     3.63       2541 1.00
    ## time01               0.26      0.34    -0.39     0.91       2465 1.00
    ## treatment            0.02      0.32    -0.61     0.65       2189 1.00
    ## time01:treatment     0.96      0.48     0.01     1.88       2274 1.00
    ## 
    ## Family Specific Parameters: 
    ##       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
    ## sigma     1.35      0.04     1.28     1.42       4000 1.00
    ## 
    ## Samples were drawn using sampling(NUTS). For each parameter, Eff.Sample 
    ## is a crude measure of effective sample size, and Rhat is the potential 
    ## scale reduction factor on split chains (at convergence, Rhat = 1).

What’s happened here is we refitted the same model to our new fake
dataset, and in this instance, we attained the goal of rejecting 0 from
the `time01:treatment` 95% uncertainty interval (Step 4).

In order to estimate power, we need to do these steps over and over
again. Refitting this model takes about 5 seconds, so doing our power
analysis with, for example, 500 iterations will take about 45 minutes.

To repeatedly perform the above steps, I am just going to wrap the steps
2-3 into a for-loop. I’ll also wrap the for-loop into an ifelse
statement that saves the simulation unless it is already found on disk.
(If you clone this repo from gitlab, be warned as this will take a while
because I’m not pushing the saved results to gitlab.)

``` r
# Set up simulation parameters
nsims <- 500
alpha <- .05  # argh
```

``` r
if (!file.exists("brms.rda")) {
  # Take nsims rows of hypothetical parameter values
  samples <- sample(1:nsamples(fit_brm), nsims)
  # Generate nsims vectors of fake intimacy ratings
  preds <- predict(fit_brm, summary = F, subset = samples)
  # Prepare a list for the results of the simulation
  res <- vector("list", nsims)
  
  for (i in 1:nsims) {
    # 2. Create one fake dataset
    dat_new <- dat
    dat_new$intimacy <- preds[i,]
    # 3. Get model's posterior based on fake dataset
    fit_fake <- update(
      fit_brm, 
      iter = 1000,
      save_ranef = F,
      newdata = dat_new,
      recompile = F
    )
    # Save posterior samples of time01:treatment
    res[[i]] <- fixef(fit_fake, summary = F)[, 4]
    
  }
  save(res, file = "brms.rda")
} else {load("brms.rda")}
```

After the simulation has been run, we calculate the proportion of
iterations for which the goal (exclude 0 from 95% uncertainty interval)
was attained. We use `lapply()` to get the lower 2.5%ile of each vector,
and then check whether it’s greater than zero.

``` r
q2.5 <- lapply(res, quantile, probs = .025)
attained <- sum(q2.5 > 0.0)
p <- sum(q2.5 > 0.0)/nsims
# CI with normal approximation
ci <- round(p + c(-1.96, 1.96) * sqrt(p * (1 - p) / nsims), 2)
data.frame(
  iterations = nsims, 
  attained, 
  power = p,
  ci.lwr = ci[1],
  ci.upr = ci[2]
)
```

    ##   iterations attained power ci.lwr ci.upr
    ## 1        500      240  0.48   0.44   0.52

Our power estimate is very close to the frequentist one reported above.
So far, we have shown how to reproduce a frequentist (\(\alpha = .05\)
or bust) power analysis with bayesian methods using the **brms** R
package (Bürkner 2017). Future updates will include varying sample sizes
(subjects and time points) to find out how to get to a specific level of
power (e.g. 80%), and examples of other goals.

# References

<div id="refs" class="references">

<div id="ref-BolgerIntensiveLongitudinalMethods2013">

Bolger, Niall, and Jean-Philippe Laurenceau. 2013. *Intensive
Longitudinal Methods: An Introduction to Diary and Experience Sampling
Research*. New York, NY: Guilford Press.
<http://www.intensivelongitudinal.com/>.

</div>

<div id="ref-burkner_brms:_2017">

Bürkner, Paul-Christian. 2017. “Brms: An R Package for Bayesian
Multilevel Models Using Stan.” *Journal of Statistical Software* 80 (1):
1–28. <https://doi.org/10.18637/jss.v080.i01>.

</div>

<div id="ref-GreenSIMRpackagepower2015">

Green, Peter, and Catriona J. MacLeod. 2015. “SIMR: An R Package for
Power Analysis of Generalized Linear Mixed Models by Simulation.”
*Methods in Ecology and Evolution* 7 (4): 493–98.
<https://doi.org/10.1111/2041-210X.12504>.

</div>

<div id="ref-kruschke_doing_2014">

Kruschke, John K. 2014. *Doing Bayesian Data Analysis: A Tutorial
Introduction with R*. 2nd Edition. Burlington, MA: Academic Press.

</div>

</div>
